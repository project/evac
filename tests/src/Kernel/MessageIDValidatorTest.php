<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Kernel;

use Drupal\evac\Utility\MessageIDValidator;
use Drupal\KernelTests\KernelTestBase;
use Egulias\EmailValidator\Validation\MessageIDValidation;

/**
 * Tests the MessageIDValidator utility class.
 *
 * @coversDefaultClass \Drupal\evac\Utility\MessageIDValidator
 * @group evac
 */
class MessageIDValidatorTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'evac',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['evac']);
  }

  /**
   * @covers ::isValid
   */
  public function testIsValid(): void {
    // Note that \Drupal\Component\Utility\EmailValidator wraps
    // \Egulias\EmailValidator\EmailValidator, so we don't do anything more than
    // test that the wrapping works since the dependency has its own test
    // coverage.
    $validator = new MessageIDValidator();

    // The examples are copied from
    // \Egulias\EmailValidator\Tests\EmailValidator\Validation\MessageIDValidationTest.
    $valid = 'a@b.c+&%$.d';
    $this->assertTrue($validator->isValid($valid), "Email $valid should pass.");

    $invalid = 'example@ia\na.';
    $this->assertFalse($validator->isValid($invalid), "Email $invalid should fail.");
  }

  /**
   * @covers ::isValid
   */
  public function testIsValidException(): void {
    $validator = new MessageIDValidator();
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Calling \Drupal\evac\Utility\MessageIDValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    $validator->isValid('example@example.com', (new MessageIDValidation()));
  }

}
