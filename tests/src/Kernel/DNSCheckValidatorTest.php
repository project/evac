<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Kernel;

use Drupal\evac\Utility\DNSCheckValidator;
use Drupal\KernelTests\KernelTestBase;
use Egulias\EmailValidator\Validation\DNSCheckValidation;

/**
 * Tests the DNSCheckValidator utility class.
 *
 * @coversDefaultClass \Drupal\evac\Utility\DNSCheckValidator
 * @group evac
 */
class DNSCheckValidatorTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'evac',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['evac']);
  }

  /**
   * @covers ::isValid
   */
  public function testIsValid(): void {
    // Note that \Drupal\Component\Utility\EmailValidator wraps
    // \Egulias\EmailValidator\EmailValidator, so we don't do anything more than
    // test that the wrapping works since the dependency has its own test
    // coverage.
    $validator = new DNSCheckValidator();

    // Some examples are copied from
    // \Egulias\EmailValidator\Tests\EmailValidator\Validation\DNSCheckValidationTest.
    // Domain with MX records.
    $valid = 'Abc@ietf.org';
    $this->assertTrue($validator->isValid($valid), "Email $valid should pass DNS check.");

    // Domain without MX records.
    $invalid = 'nullmx@example.com';
    $this->assertFalse($validator->isValid($invalid), "Email $invalid should fail DNS check.");

    // Domain without TLD.
    $invalid = 'example@someinternalhost';
    $this->assertFalse($validator->isValid($invalid), "Email $invalid should fail due to missing TLD.");

    // Domain with reserved TLD.
    $invalid = 'example@example.test';
    $this->assertFalse($validator->isValid($invalid), "Email $invalid should fail due to reserved TLD.");
  }

  /**
   * @covers ::isValid
   */
  public function testIsValidException(): void {
    $validator = new DNSCheckValidator();
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Calling \Drupal\evac\Utility\DNSCheckValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    $validator->isValid('example@example.com', (new DNSCheckValidation()));
  }

}
