<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Kernel;

use Drupal\evac\Utility\NoRFCWarningsValidator;
use Drupal\KernelTests\KernelTestBase;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;

/**
 * Tests the NoRFCWarningsValidator utility class.
 *
 * @coversDefaultClass \Drupal\evac\Utility\NoRFCWarningsValidator
 * @group evac
 */
class NoRFCWarningsValidatorTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'evac',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['evac']);
  }

  /**
   * @covers ::isValid
   */
  public function testIsValid(): void {
    // Note that \Drupal\Component\Utility\EmailValidator wraps
    // \Egulias\EmailValidator\EmailValidator, so we don't do anything more than
    // test that the wrapping works since the dependency has its own test
    // coverage.
    $validator = new NoRFCWarningsValidator();

    // Examples are copied from
    // \Egulias\EmailValidator\Tests\EmailValidator\Validation\NoRFCWarningsValidationTest.
    $valid = 'example@example.com';
    $this->assertTrue($validator->isValid($valid), "Email $valid should pass.");

    $invalid = 'test()@example.com';
    $this->assertFalse($validator->isValid($invalid), "Email $invalid should fail.");
  }

  /**
   * @covers ::isValid
   */
  public function testIsValidException(): void {
    $validator = new NoRFCWarningsValidator();
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Calling \Drupal\evac\Utility\NoRFCWarningsValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    $validator->isValid('example@example.com', (new NoRFCWarningsValidation()));
  }

}
