<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Kernel;

use Drupal\evac\Utility\SpoofCheckValidator;
use Drupal\KernelTests\KernelTestBase;
use Egulias\EmailValidator\Validation\Extra\SpoofCheckValidation;

/**
 * Tests the SpoofCheckValidator utility class.
 *
 * @coversDefaultClass \Drupal\evac\Utility\SpoofCheckValidator
 * @group evac
 */
class SpoofCheckValidatorTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = [
    'evac',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['evac']);
  }

  /**
   * @covers ::isValid
   */
  public function testIsValid(): void {
    // Note that \Drupal\Component\Utility\EmailValidator wraps
    // \Egulias\EmailValidator\EmailValidator, so we don't do anything more than
    // test that the wrapping works since the dependency has its own test
    // coverage.
    $validator = new SpoofCheckValidator();

    // The examples are copied from
    // \Egulias\EmailValidator\Tests\EmailValidator\Validation\Extra\SpoofCheckValidationTest.
    // Cyrillic.
    $valid = 'Кириллица@Кириллица';
    $this->assertTrue($validator->isValid($valid), "Email $valid should pass spoof check.");

    // Mixed.
    $invalid = 'Кириллицаlatin漢字ひらがなカタカナ';
    $this->assertFalse($validator->isValid($invalid), "Email $invalid should fail DNS check.");
  }

  /**
   * @covers ::isValid
   */
  public function testIsValidException(): void {
    $validator = new SpoofCheckValidator();
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Calling \Drupal\evac\Utility\SpoofCheckValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    $validator->isValid('example@example.com', (new SpoofCheckValidation()));
  }

}
