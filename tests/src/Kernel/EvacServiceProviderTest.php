<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Test the replacement of the core Email Validator service.
 *
 * @coversDefaultClass \Drupal\evac\EvacServiceProvider
 * @group evac
 */
class EvacServiceProviderTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var string[]
   */
  protected static $modules = ['evac'];

  /**
   * Test if core Email Validator service has been replaced.
   *
   * @covers ::alter
   */
  public function testAlter(): void {

    $definition = $this->container->getDefinition('email.validator');
    $this->assertSame('Drupal\\evac\\Utility\\ReplacementSwitcher', $definition->getClass(), 'Class has been changed');
  }

  /**
   * Test if all the additional Email Validator services are present + correct.
   */
  public function testEmailValidatorServices(): void {

    $definition = $this->container->getDefinition('email.validator.dns_check');
    $this->assertSame('Drupal\\evac\\Utility\\DNSCheckValidator', $definition->getClass(), 'DNSCheckValidator OK.');

    $definition = $this->container->getDefinition('email.validator.message_id');
    $this->assertSame('Drupal\\evac\\Utility\\MessageIDValidator', $definition->getClass(), 'MessageIDValidator OK.');

    $definition = $this->container->getDefinition('email.validator.multiple_with_and');
    $this->assertSame('Drupal\\evac\\Utility\\MultipleValidationWithAndValidator', $definition->getClass(), 'MultipleValidationWithAndValidator OK.');

    $definition = $this->container->getDefinition('email.validator.no_rfc_warnings');
    $this->assertSame('Drupal\\evac\\Utility\\NoRFCWarningsValidator', $definition->getClass(), 'NoRFCWarningsValidator OK.');

    $definition = $this->container->getDefinition('email.validator.spoof_check');
    $this->assertSame('Drupal\\evac\\Utility\\SpoofCheckValidator', $definition->getClass(), 'SpoofCheckValidator OK.');
  }

}
