<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Unit\Utility;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\evac\Utility\DNSCheckValidator;
use Drupal\evac\Utility\MessageIDValidator;
use Drupal\evac\Utility\NoRFCWarningsValidator;
use Drupal\evac\Utility\ReplacementSwitcher;
use Drupal\evac\Utility\SpoofCheckValidator;
use Drupal\Tests\UnitTestCase;
use Egulias\EmailValidator\Validation\RFCValidation;

/**
 * Tests the ReplacementSwitcher utility class.
 *
 * @coversDefaultClass \Drupal\evac\Utility\ReplacementSwitcher
 * @group evac
 */
class ReplacementSwitcherTest extends UnitTestCase {

  /**
   * Config factory mock.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Default module config.
    $this->configFactory = $this->getConfigFactoryMock();
  }

  /**
   * @covers ::isValid
   */
  public function testIsValid(): void {
    // Note that \Drupal\Component\Utility\EmailValidator wraps
    // \Egulias\EmailValidator\EmailValidator, so we don't do anything more than
    // test that the wrapping works since the dependency has its own test
    // coverage.
    $validator = new ReplacementSwitcher($this->configFactory);
    $this->assertTrue($validator->isValid('example@example.com'));
  }

  /**
   * @covers ::isValid
   */
  public function testIsValidException(): void {
    $validator = new ReplacementSwitcher($this->configFactory);
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Calling \Drupal\evac\Utility\ReplacementSwitcher::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    $validator->isValid('example@example.com', (new RFCValidation()));
  }

  /**
   * @covers ::getReplacementValidator
   */
  public function testGetReplacementValidator(): void {

    // No replacement.
    $validator = new ReplacementSwitcher($this->configFactory);
    $this->assertEquals(NULL, $validator->getReplacementValidator());

    $config_factory = $this->getConfigFactoryMock(TRUE, 'dns_check');
    $validator = new ReplacementSwitcher($config_factory);
    $this->assertInstanceOf(DNSCheckValidator::class, $validator->getReplacementValidator());

    $config_factory = $this->getConfigFactoryMock(TRUE, 'message_id');
    $validator = new ReplacementSwitcher($config_factory);
    $this->assertInstanceOf(MessageIDValidator::class, $validator->getReplacementValidator());

    $config_factory = $this->getConfigFactoryMock(TRUE, 'no_rfc_warnings');
    $validator = new ReplacementSwitcher($config_factory);
    $this->assertInstanceOf(NoRFCWarningsValidator::class, $validator->getReplacementValidator());

    $config_factory = $this->getConfigFactoryMock(TRUE, 'spoof_check');
    $validator = new ReplacementSwitcher($config_factory);
    $this->assertInstanceOf(SpoofCheckValidator::class, $validator->getReplacementValidator());
  }

  /**
   * Get a config factory mock, containing config mock for evac.settings.
   *
   * @param bool $replace
   *   The value for 'replace'.
   * @param string $replacement
   *   The value for 'replacement'.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The mock.
   */
  protected function getConfigFactoryMock(bool $replace = FALSE, string $replacement = ''): ConfigFactoryInterface {

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->getConfigFactoryStub([
      'evac.settings' => [
        'replace' => $replace,
        'replacement' => $replacement,
      ],
    ]);

    return $config_factory;
  }

}
