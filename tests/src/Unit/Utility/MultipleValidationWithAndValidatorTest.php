<?php

declare(strict_types=1);

namespace Drupal\Tests\evac\Unit\Utility;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\evac\Utility\MultipleValidationWithAndValidator;
use Drupal\Tests\UnitTestCase;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\Extra\SpoofCheckValidation;
use Egulias\EmailValidator\Validation\MessageIDValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;
use Egulias\EmailValidator\Validation\RFCValidation;

/**
 * Tests the MultipleValidationWithAndValidator utility class.
 *
 * @coversDefaultClass \Drupal\evac\Utility\MultipleValidationWithAndValidator
 * @group evac
 */
class MultipleValidationWithAndValidatorTest extends UnitTestCase {

  /**
   * Config factory mock.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Default module config.
    $this->configFactory = $this->getConfigFactoryMock();
  }

  /**
   * @covers ::isValid
   */
  public function testIsValid(): void {
    // Note that \Drupal\Component\Utility\EmailValidator wraps
    // \Egulias\EmailValidator\EmailValidator, so we don't do anything more than
    // test that the wrapping works since the dependency has its own test
    // coverage.
    // @see \Drupal\Tests\evac\Unit\Utility\DNSCheckValidatorTest::testIsValid
    // for DNS check examples.
    $validator = new MultipleValidationWithAndValidator($this->configFactory);
    $this->assertTrue($validator->isValid('Abc@ietf.org'));
  }

  /**
   * @covers ::isValid
   */
  public function testIsValidException(): void {
    $validator = new MultipleValidationWithAndValidator($this->configFactory);
    $this->expectException(\BadMethodCallException::class);
    $this->expectExceptionMessage('Calling \Drupal\evac\Utility\MultipleValidationWithAndValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    $validator->isValid('example@example.com', (new MultipleValidationWithAnd($validator->getValidations())));
  }

  /**
   * @covers ::getValidations
   */
  public function testGetReplacementValidator(): void {

    // Defaults.
    $expected = [
      DNSCheckValidation::class,
      SpoofCheckValidation::class,
    ];
    $validator = new MultipleValidationWithAndValidator($this->configFactory);
    $validations = $validator->getValidations();
    foreach ($validations as $index => $validation) {
      $this->assertInstanceOf($expected[$index], $validation);
    }

    // All options.
    $config = [
      'dns_check' => 'dns_check',
      'message_id' => 'message_id',
      'no_rfc_warnings' => 'no_rfc_warnings',
      'rfc' => 'rfc',
      'spoof_check' => 'spoof_check',
    ];
    $config_factory = $this->getConfigFactoryMock($config);
    $expected = [
      DNSCheckValidation::class,
      MessageIDValidation::class,
      NoRFCWarningsValidation::class,
      RFCValidation::class,
      SpoofCheckValidation::class,
    ];
    $validator = new MultipleValidationWithAndValidator($config_factory);
    $validations = $validator->getValidations();
    foreach ($validations as $index => $validation) {
      $this->assertInstanceOf($expected[$index], $validation);
    }
  }

  /**
   * Get a config factory mock, containing config mock for evac.settings.
   *
   * @param array $validations
   *   The validations to apply.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The mock.
   */
  protected function getConfigFactoryMock(array $validations = []): ConfigFactoryInterface {

    if (empty($validations)) {

      // Defaults as in config/install/evac.settings.yml.
      $validations = [
        'dns_check' => 'dns_check',
        'message_id' => '0',
        'no_rfc_warnings' => '0',
        'rfc' => '0',
        'spoof_check' => 'spoof_check',
      ];
    }

    /** @var \Drupal\Core\Config\ConfigFactoryInterface $config_factory */
    $config_factory = $this->getConfigFactoryStub([
      'evac.settings' => [
        'replace' => TRUE,
        'replacement' => 'multiple_with_and',
        'multiple_with_and' => $validations,
      ],
    ]);

    return $config_factory;
  }

}
