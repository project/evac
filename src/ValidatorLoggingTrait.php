<?php

declare(strict_types=1);

namespace Drupal\evac;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Egulias\EmailValidator\EmailValidator;
use Egulias\EmailValidator\Result\InvalidEmail;
use Psr\Log\LoggerInterface;

/**
 * Logging helper.
 */
trait ValidatorLoggingTrait {

  use ValidatorConfigTrait;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface|null
   */
  protected ?LoggerInterface $logger = NULL;

  /**
   * Get the logger.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  protected function getLogger(): LoggerInterface {

    if (is_null($this->logger)) {
      $this->logger = \Drupal::service('logger.channel.evac');
    }

    return $this->logger;
  }

  /**
   * Log errors and warnings for given email address.
   *
   * @param string $email
   *   The validated email address.
   * @param \Egulias\EmailValidator\EmailValidator $validator
   *   The validator.
   * @param \Drupal\Core\Config\ImmutableConfig|null $config
   *   The module config. Recommend to pass along if already present in the
   *   calling function.
   * @param \Drupal\Core\Config\ConfigFactoryInterface|null $config_factory
   *   The config factory. Recommend to pass along if already present in the
   *   calling function.
   */
  protected function log(string $email, EmailValidator $validator, ImmutableConfig $config = NULL, ConfigFactoryInterface $config_factory = NULL): void {

    if (is_null($config)) {

      if (is_null($config_factory)) {
        $config_factory = \Drupal::configFactory();
      }
      $config = $this->getConfig($config_factory);
    }

    if ($config->get('log_errors')) {

      $params = [
        'email' => $email,
      ];

      if ($config->get('log_warnings')) {

        $warnings = $validator->getWarnings();
        if (!empty($warnings)) {

          $params['warnings'] = implode(', ', $warnings);
          $this->getLogger()->warning('{email}: {warnings}.', $params);
        }
      }

      $error = $validator->getError();
      if ($error instanceof InvalidEmail) {

        $params['error'] = $error->reason()->description();
        $this->getLogger()->error('{email}: {error}.', $params);
      }
    }
  }

}
