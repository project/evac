<?php

declare(strict_types=1);

namespace Drupal\evac;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\evac\Utility\DNSCheckValidator;
use Drupal\evac\Utility\MessageIDValidator;
use Drupal\evac\Utility\MultipleValidationWithAndValidator;
use Drupal\evac\Utility\NoRFCWarningsValidator;
use Drupal\evac\Utility\SpoofCheckValidator;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\EmailValidation;
use Egulias\EmailValidator\Validation\Extra\SpoofCheckValidation;
use Egulias\EmailValidator\Validation\MessageIDValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;
use Egulias\EmailValidator\Validation\NoRFCWarningsValidation;
use Egulias\EmailValidator\Validation\RFCValidation;

/**
 * Combines Validation-Validator service mapping and helpers.
 */
trait ValidatorInfoTrait {

  /**
   * Get the Validation class for given config key.
   *
   * @param string $config_key
   *   The config key.
   *
   * @return \Egulias\EmailValidator\Validation\EmailValidation|null
   *   The Validation class.
   */
  protected function getValidation(string $config_key): ?EmailValidation {

    $validation_class = $this->validatorMapping()[$config_key]['validation'] ?? NULL;
    if (!is_null($validation_class) && class_exists($validation_class)) {
      /** @var \Egulias\EmailValidator\Validation\EmailValidation $validation */
      $validation = new $validation_class();
      return $validation;
    }

    return NULL;
  }

  /**
   * Get the Validator Service class for given config key.
   *
   * @param string $config_key
   *   The config key.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   *
   * @return \Drupal\Component\Utility\EmailValidatorInterface|null
   *   The Validator Service class.
   */
  protected function getValidatorService(string $config_key, ConfigFactoryInterface $config_factory): ?EmailValidatorInterface {

    $validator_class = $this->validatorMapping()[$config_key]['service'] ?? NULL;
    if (!is_null($validator_class) && class_exists($validator_class)) {
      /** @var \Drupal\Component\Utility\EmailValidatorInterface $validator */
      $validator = new $validator_class($config_factory);
      return $validator;
    }

    return NULL;
  }

  /**
   * List of all available Validations for MultipleValidationWithAnd.
   *
   * @return string[]
   *   Replacement Validations keyed by the last part of their corresponding
   *   Validator service system name.
   */
  protected function multipleWithAndOptions(): array {

    $replacements = $this->validationNames();
    unset($replacements['multiple_with_and']);

    return $replacements;
  }

  /**
   * List of available replacement Validation names.
   *
   * @return string[]
   *   Replacement Validation names keyed by the last part of their
   *   corresponding Validator service system name.
   */
  protected function validationNames(): array {

    return array_map(
      function ($validation) {
        return $validation['name'];
      },
      $this->validatorMapping()
    );
  }

  /**
   * Validation-Validator service mapping.
   *
   * @return string[][]
   *   The Validation-Validator service mapping keyed by their config key.
   */
  protected function validatorMapping(): array {
    return [
      'dns_check' => [
        'service' => DNSCheckValidator::class,
        'validation' => DNSCheckValidation::class,
        'name' => 'DNSCheckValidation',
      ],
      'message_id' => [
        'service' => MessageIDValidator::class,
        'validation' => MessageIDValidation::class,
        'name' => 'MessageIDValidation',
      ],
      'multiple_with_and' => [
        'service' => MultipleValidationWithAndValidator::class,
        'validation' => MultipleValidationWithAnd::class,
        'name' => 'MultipleValidationWithAnd',
      ],
      'no_rfc_warnings' => [
        'service' => NoRFCWarningsValidator::class,
        'validation' => NoRFCWarningsValidation::class,
        'name' => 'NoRFCWarningValidation',
      ],
      'rfc' => [
        'service' => EmailValidator::class,
        'validation' => RFCValidation::class,
        'name' => 'RFCValidation',
      ],
      'spoof_check' => [
        'service' => SpoofCheckValidator::class,
        'validation' => SpoofCheckValidation::class,
        'name' => 'SpoofCheckValidation',
      ],
    ];
  }

}
