<?php

declare(strict_types=1);

namespace Drupal\evac\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\RedundantEditableConfigNamesTrait;
use Drupal\evac\ValidatorConfigTrait;
use Drupal\evac\ValidatorInfoTrait;

/**
 * The Email Validator Customizer Config Form.
 */
class ConfigForm extends ConfigFormBase {

  use RedundantEditableConfigNamesTrait;
  use ValidatorConfigTrait;
  use ValidatorInfoTrait;

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'evac_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form = parent::buildForm($form, $form_state);

    $form['replace'] = [
      '#title' => $this->t('Replace the Email Validator service from core (RFCValidation).'),
      '#type' => 'checkbox',
      '#config_target' => 'evac.settings:replace',
    ];

    $params = [
      ':url' => 'https://github.com/egulias/EmailValidator#available-validations',
    ];
    $replacements = $this->validationNames();
    // Remove the validation we intend to replace.
    unset($replacements['rfc']);
    $form['replacement'] = [
      '#title' => $this->t('Replacement validation'),
      '#type' => 'radios',
      '#description' => $this->t('Not sure which validation to choose? The <a href=":url" target="_blank" title=">egulias/EmailValidator">egulias/EmailValidator</a> documentation provides an overview of the various validation types.', $params),
      '#options' => $replacements,
      '#config_target' => 'evac.settings:replacement',
      '#states' => [
        'enabled' => [
          [':input[name="replace"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    $params = [
      '@method' => $this->validationNames()['multiple_with_and'],
    ];
    $form['multiple_with_and'] = [
      '#title' => $this->t('Validations for @method', $params),
      '#type' => 'checkboxes',
      '#options' => $this->multipleWithAndOptions(),
      '#config_target' => 'evac.settings:multiple_with_and',
      '#states' => [
        'visible' => [
          [':input[name="replacement"]' => ['value' => 'multiple_with_and']],
        ],
        'enabled' => [
          [':input[name="replace"]' => ['checked' => TRUE]],
        ],

        // This required doesn't actually work as expected, so we have to check
        // in form validation.
        // @see https://www.drupal.org/project/drupal/issues/2950999
        'required' => [
          [':input[name="replacement"]' => ['value' => 'multiple_with_and']],
        ],
      ],
    ];

    $form['log_errors'] = [
      '#title' => $this->t('Log validation errors.'),
      '#description' => $this->t('Logs every validation error. Might generate a lot of records, depending in your case. Might be useful on production, but use with care.'),
      '#type' => 'checkbox',
      '#config_target' => 'evac.settings:log_errors',
    ];

    $form['log_warnings'] = [
      '#title' => $this->t('Log validation warnings.'),
      '#type' => 'checkbox',
      '#description' => $this->t('Logs every validation warning. Might generate even more records. Not recommended for production.'),
      '#config_target' => 'evac.settings:log_warnings',
      '#states' => [
        'enabled' => [
          [':input[name="log_errors"]' => ['checked' => TRUE]],
        ],
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {

    $values = $form_state->getValues();
    if ($values['replace'] === 1 && $values['replacement'] === 'multiple_with_and') {

      $options = $values['multiple_with_and'];
      $options = array_flip($options);
      unset($options[0]);
      if (count($options) < 1) {

        $params = [
          '@method' => $this->validationNames()['multiple_with_and'],
        ];
        $message = $this->t('Choose at least 1 validation for @method to use.', $params);
        $form_state->setErrorByName('multiple_with_and', $message->render());
      }
    }

    parent::validateForm($form, $form_state);
  }

}
