<?php

declare(strict_types=1);

namespace Drupal\evac;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;

/**
 * Module config helper.
 */
trait ValidatorConfigTrait {

  /**
   * Returns the module config key.
   *
   * @return string
   *   The config key.
   */
  protected function configKey(): string {
    return 'evac.settings';
  }

  /**
   * Get the module config.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface|null $config_factory
   *   The config factory.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The module config.
   */
  protected function getConfig(ConfigFactoryInterface $config_factory = NULL): ImmutableConfig {

    if (is_null($config_factory)) {
      $config_factory = \Drupal::configFactory();
    }

    return $config_factory->get($this->configKey());
  }

}
