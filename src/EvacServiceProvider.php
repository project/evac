<?php

declare(strict_types=1);

namespace Drupal\evac;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;
use Drupal\Core\DependencyInjection\ServiceProviderInterface;
use Drupal\evac\Utility\ReplacementSwitcher;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Overrides the 'email.validator' service.
 */
class EvacServiceProvider extends ServiceProviderBase implements ServiceProviderInterface {

  /**
   * {@inheritdoc}
   */
  public function alter(ContainerBuilder $container): void {

    $definition = $container->getDefinition('email.validator');
    if ($definition->getClass() === EmailValidator::class) {

      $definition->setClass(ReplacementSwitcher::class);
      $definition->addArgument(new Reference('config.factory'));
    }
  }

}
