<?php

declare(strict_types=1);

namespace Drupal\evac\Utility;

use Drupal\Component\Utility\EmailValidator;
use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\evac\ValidatorConfigTrait;
use Drupal\evac\ValidatorInfoTrait;
use Drupal\evac\ValidatorLoggingTrait;
use Egulias\EmailValidator\Validation\EmailValidation;

/**
 * Replace the Email Validator with one of the replacements or default if none.
 */
class ReplacementSwitcher implements EmailValidatorInterface {

  use ValidatorConfigTrait;
  use ValidatorInfoTrait;
  use ValidatorLoggingTrait;

  /**
   * Module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->config = $config_factory->get($this->configKey());
    $this->configFactory = $config_factory;
  }

  /**
   * Validates an email address.
   *
   * @param string $email
   *   A string containing an email address.
   * @param \Egulias\EmailValidator\Validation\EmailValidation|null $emailValidation
   *   This argument is ignored. If it is supplied an error will be triggered.
   *   See https://www.drupal.org/node/2997196.
   *
   * @return bool
   *   TRUE if the address is valid.
   */
  public function isValid($email, EmailValidation $emailValidation = NULL): bool {

    if ($emailValidation) {
      throw new \BadMethodCallException('Calling \Drupal\evac\Utility\ReplacementSwitcher::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    }

    $validator = $this->getReplacementValidator();

    // Fallback to default from Core.
    if ($validator === NULL) {
      $validator = new EmailValidator();
    }

    $is_valid = $validator->isValid($email);

    // Only need to log for the default validator. The others handle it on their
    // own.
    if (!$is_valid && $validator instanceof EmailValidator) {
      $this->log($email, $validator, $this->config);
    }

    return $is_valid;
  }

  /**
   * Get the replacement Validator.
   *
   * @return \Drupal\Component\Utility\EmailValidatorInterface|null
   *   The Validator Service class or NULL if replacement is disabled.
   */
  public function getReplacementValidator(): ?EmailValidatorInterface {

    $validator = NULL;
    if ($this->config->get('replace')) {
      $validator = $this->getValidatorService($this->config->get('replacement'), $this->configFactory);
    }

    return $validator;
  }

}
