<?php

declare(strict_types=1);

namespace Drupal\evac\Utility;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\evac\ValidatorLoggingTrait;
use Egulias\EmailValidator\EmailValidator as EmailValidatorUtility;
use Egulias\EmailValidator\Validation\DNSCheckValidation;
use Egulias\EmailValidator\Validation\EmailValidation;

/**
 * Validates email addresses.
 */
class DNSCheckValidator extends EmailValidatorUtility implements EmailValidatorInterface {

  use ValidatorLoggingTrait;

  /**
   * Validates an email address.
   *
   * @param string $email
   *   A string containing an email address.
   * @param \Egulias\EmailValidator\Validation\EmailValidation|null $emailValidation
   *   This argument is ignored. If it is supplied an error will be triggered.
   *   See https://www.drupal.org/node/2997196.
   *
   * @return bool
   *   TRUE if the address is valid.
   */
  public function isValid($email, EmailValidation $emailValidation = NULL): bool {
    if ($emailValidation) {
      throw new \BadMethodCallException('Calling \Drupal\evac\Utility\DNSCheckValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    }

    $is_valid = parent::isValid($email, (new DNSCheckValidation()));
    if (!$is_valid) {
      $this->log($email, $this);
    }

    return $is_valid;
  }

}
