<?php

declare(strict_types=1);

namespace Drupal\evac\Utility;

use Drupal\Component\Utility\EmailValidatorInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\evac\ValidatorInfoTrait;
use Drupal\evac\ValidatorLoggingTrait;
use Egulias\EmailValidator\EmailValidator as EmailValidatorUtility;
use Egulias\EmailValidator\Validation\EmailValidation;
use Egulias\EmailValidator\Validation\MultipleValidationWithAnd;

/**
 * Validates email addresses.
 */
class MultipleValidationWithAndValidator extends EmailValidatorUtility implements EmailValidatorInterface {

  use ValidatorInfoTrait;
  use ValidatorLoggingTrait;

  /**
   * The module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    parent::__construct();
    $this->config = $config_factory->get($this->configKey());
  }

  /**
   * Validates an email address.
   *
   * @param string $email
   *   A string containing an email address.
   * @param \Egulias\EmailValidator\Validation\EmailValidation|null $emailValidation
   *   This argument is ignored. If it is supplied an error will be triggered.
   *   See https://www.drupal.org/node/2997196.
   *
   * @return bool
   *   TRUE if the address is valid.
   *
   * @throws \Egulias\EmailValidator\Validation\Exception\EmptyValidationList
   */
  public function isValid($email, EmailValidation $emailValidation = NULL): bool {
    if ($emailValidation) {
      throw new \BadMethodCallException('Calling \Drupal\evac\Utility\MultipleValidationWithAndValidator::isValid() with the second argument is not supported. See https://www.drupal.org/node/2997196');
    }

    $validations = $this->getValidations();
    $is_valid = parent::isValid($email, (new MultipleValidationWithAnd($validations)));
    if (!$is_valid) {
      $this->log($email, $this, $this->config);
    }

    return $is_valid;
  }

  /**
   * Get the Validations be used by this Validator.
   *
   * @return \Egulias\EmailValidator\Validation\EmailValidation[]
   *   The Validations.
   */
  public function getValidations(): array {

    $validations = [];
    foreach ($this->config->get('multiple_with_and') as $key => $enabled) {

      if ((string) $enabled != '0') {

        $validator = $this->getValidation($key);
        if ($validator !== NULL) {
          $validations[] = $validator;
        }
      }
    }

    return $validations;
  }

}
