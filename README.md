CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

Drupal Core uses the RFCValidation from [egulias/EmailValidator](https://github.com/egulias/EmailValidator)
to validate email addresses. However, the RFC allows for email addresses without
domain, see issue [Drupal email.validator service isValid accepts emails with no domain](https://www.drupal.org/project/drupal/issues/2822142),
which is often not the desired behaviour.

This module implements all other Validations provided by [egulias/EmailValidator](https://github.com/egulias/EmailValidator)
as services. More importantly, on the module configuration page, you can replace
the Core service (RFCValidation) with any of your choosing or even combine
multiple using the MultipleValidationWithAnd validation.

Included Validations:

 * DNSCheckValidation (default)
 * MessageIDValidation
 * MultipleValidationWithAnd
 * NoRFCWarningValidation
 * RFCValidation, same as core, so only as option for MultipleValidationWithAnd.
 * SpoofCheckValidation

DNSCheckValidation is the default replacement Validation.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  [Installing Modules](https://www.drupal.org/node/1897420) for further
  information.



CONFIGURATION
-------------

 * Install this module, go to the module configuration page.
 * Update the validation options as desired.

Configurable parameters:
 * __Replace the Email Validator service from core (RFCValidation)__: Uncheck if
   you only want to use the validation services
   for your custom code and leave the default validation intact.
 * __Replacement validation__: the replacement validation to use.
   _DNSCheckValidation_ is the default option, as it enforces both presence of a
   Top Level Domain and MX records for that domain.
 * __Validations for MultipleValidationWithAnd__: If _MultipleValidationWithAnd_
   is chosen in the previous option, you can select multiple validations to use.
   [egulias/EmailValidator](https://github.com/egulias/EmailValidator#available-validations)
   explains the various validation types.
 * Error and warning logging: Probably not recommended for production, use at your
   own discretion.
